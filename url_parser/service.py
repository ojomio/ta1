import functools
import re
import sys
from datetime import timedelta, datetime
from http.client import NOT_FOUND

from pyquery import PyQuery
from sqlalchemy import create_engine
from sqlalchemy.exc import ProgrammingError
from sqlalchemy.ext.declarative import declarative_base, DeferredReflection
from sqlalchemy.orm import sessionmaker, relationship
from tornado import options
from tornado.gen import coroutine
from tornado.httpclient import AsyncHTTPClient, HTTPError
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.web import Application, RequestHandler

Base = declarative_base()


class URLQueue(Base, DeferredReflection):
    __tablename__ = 'url_scheduler_urlqueue'


class URLResult(Base, DeferredReflection):
    __tablename__ = 'url_scheduler_urlparseresult'
    urlqueue = relationship('URLQueue')


class ScheduleHandler(RequestHandler):
    def get(self, *args, **kwargs):
        schedule_id = self.get_argument('schedule_id')
        schedule_item = self.settings['session'].query(URLQueue).get(schedule_id)
        if not schedule_item:
            raise HTTPError(NOT_FOUND)
        schedule_item.status = 'p'
        IOLoop.current().add_timeout(
            timedelta(seconds=int(schedule_item.timeshift)),
            functools.partial(
                self.download_and_parse,
                schedule_item,
            )
        )
        try:
            self.settings['session'].commit()
        except ProgrammingError:
            self.settings['session'].rollback()

        self.finish('%s was queued at %s seconds from now' % (schedule_item.url, schedule_item.timeshift))

    @coroutine
    def download_and_parse(self, schedule_item):
        try:
            try:
                resp = yield self.settings['client'].fetch(schedule_item.url)
            except HTTPError as e:
                schedule_item.status = 'f'
                schedule_item.error = '%s %s' % (type(e), str(e))
            else:
                schedule_item.status = 's'

            encoding = doc_encoding = None
            mimetype = resp.headers.get('Content-Type')
            encoding = self.encoding_from_mime(mimetype)

            # convert to str if encoding is in headers else use bytes
            pq = PyQuery(resp.body.decode(encoding) if encoding else resp.body)
            encoding_nodes = pq('meta[charset]')  # encoding form <meta> tag (html5)
            if encoding_nodes:
                doc_encoding = encoding_nodes[0].get('charset')
            else:
                encoding_nodes = pq('meta[http-equiv="Content-Type"]')  # encoding form <meta> tag (html old)
                if encoding_nodes:
                    doc_encoding = self.encoding_from_mime(
                        encoding_nodes[0].get('content')
                    )

            if doc_encoding and (not encoding or encoding.lower() != doc_encoding.lower()):
                print('For url %s header encoding (%s) and meta encoding (%s) don\'t match. Using doc as override'
                      % (schedule_item.url, encoding, doc_encoding))
                encoding = doc_encoding
                pq = PyQuery(resp.body.decode(encoding))

            h1_text = title_text = None

            h1_nodes = pq('h1')
            if h1_nodes:
                h1_text = h1_nodes[0].text_content()

            title_nodes = pq('title')
            if title_nodes:
                title_text = title_nodes[0].text_content()
            else:  # Last fallback for title
                title_nodes = pq('meta[property="og:title"]')
                if title_nodes:
                    title_text = title_nodes[0].get('content')

            print(h1_text, title_text, encoding)
            schedule_item.last_outcome_date = datetime.now()
            result = URLResult(
                header=h1_text,
                title=title_text,
                encoding=encoding,
                urlqueue=schedule_item,
            )
            self.settings['session'].add(result)
            self.settings['session'].commit()
        except ProgrammingError:
            self.settings['session'].rollback()

    def encoding_from_mime(self, mimetype):
        if not mimetype:
            return
        matchobj = re.search(r';\s*charset=(.*?)(;|$)', mimetype)
        if matchobj:
            return matchobj.group(1)


def main(*args):
    return_code = 0

    options.define('address', default='localhost')
    options.define('port', default='8887')
    options.define('db_file')
    options.parse_command_line()
    engine = create_engine(
        'sqlite:///%s' % options.options.db_file
    )
    DeferredReflection.prepare(engine)  # Autoload the tables from DB schema

    application = Application(
        [
            (r'^/schedule/?', ScheduleHandler)
        ],
        client=AsyncHTTPClient(),
        session=sessionmaker(engine)(),
        debug=True,
        **options.options.as_dict()
    )
    http_server = HTTPServer(application)
    http_server.listen(application.settings['port'], application.settings['address'])
    IOLoop.current().start()

    return return_code


if __name__ == "__main__":
    main(sys.argv)
