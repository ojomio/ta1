from django.db import models
from django.db.models import Model
from django.db.models.fields import CharField, DateTimeField
from django.db.models.fields.related import ForeignKey


class UrlQueue(Model):
    url = models.URLField(verbose_name='The URL to be parsed')
    timeshift = models.CharField(max_length=5)
    status = models.CharField(max_length=50, default='q', choices=[('q','Queued'),('p','Processing'), ('s', 'Success'), ('f', 'Failure')])
    error = CharField(max_length=500, blank=True, null=True, default=None)
    last_outcome_date = DateTimeField(null=True)

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        super().save(force_insert, force_update, using, update_fields)



class UrlParseResult(Model):
    title = CharField(max_length=500, blank=True, null=True)
    header = CharField(max_length=500, blank=True, null=True)
    encoding = CharField(max_length=50, blank=True, null=True)

    url = ForeignKey(UrlQueue)
