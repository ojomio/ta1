# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('url_scheduler', '0002_urlparseresult'),
    ]

    operations = [
        migrations.AddField(
            model_name='urlqueue',
            name='error',
            field=models.CharField(null=True, blank=True, max_length=500, default=None),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='urlqueue',
            name='status',
            field=models.CharField(choices=[('q', 'Queued'), ('p', 'Processing'), ('s', 'Success'), ('f', 'Failure')], max_length=50, default='q'),
            preserve_default=True,
        ),
    ]
