# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('url_scheduler', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UrlParseResult',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, verbose_name='ID', primary_key=True)),
                ('title', models.CharField(max_length=500, null=True, blank=True)),
                ('header', models.CharField(max_length=500, null=True, blank=True)),
                ('encoding', models.CharField(max_length=50, null=True, blank=True)),
                ('url', models.ForeignKey(to='url_scheduler.UrlQueue')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
