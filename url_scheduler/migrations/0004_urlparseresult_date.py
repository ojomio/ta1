# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('url_scheduler', '0003_auto_20160130_1819'),
    ]

    operations = [
        migrations.AddField(
            model_name='urlparseresult',
            name='date',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
