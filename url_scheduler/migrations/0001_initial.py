# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='UrlQueue',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('url', models.URLField(verbose_name='The URL to be parsed')),
                ('timeshift', models.CharField(max_length=5)),
                ('status', models.CharField(choices=[('q', 'Queued'), ('p', 'Processing'), ('s', 'Success'), ('f', 'Failure')], max_length=50)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
