# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('url_scheduler', '0004_urlparseresult_date'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='urlparseresult',
            name='date',
        ),
        migrations.AddField(
            model_name='urlqueue',
            name='last_outcome_date',
            field=models.DateTimeField(null=True),
            preserve_default=True,
        ),
    ]
