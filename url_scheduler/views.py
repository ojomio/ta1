# Create your views here.
from django.views.generic.list import ListView

from url_scheduler.models import UrlQueue, UrlParseResult


class MainView(ListView):
    model = UrlQueue
    template_name = 'base.html'
    queryset = UrlQueue.objects.filter(status__in=['s', 'f']).order_by('last_outcome_date')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['successful_results'] = UrlParseResult.objects.filter(url__status='s')
        return context

