from math import floor
import django.contrib.admin
from django import forms
from django.conf import settings
from django.contrib import admin
# Register your models here.
from django.contrib.admin.options import StackedInline
from django.forms.models import ModelForm
from url_scheduler.models import UrlQueue, UrlParseResult


class UrlParseResultInline(StackedInline):
    model = UrlParseResult
    readonly_fields = ('title', 'header', 'encoding')
    extra = 0


class UrlQueueForm(ModelForm):
    # Show nice delay widget
    class Meta:
        model = UrlQueue
        exclude = ['timeshift']

    minute_delay = forms.IntegerField(min_value=0, initial=0, label='minutes',
                                      help_text='Timeshift in minutes untils the parsing')
    second_delay = forms.IntegerField(min_value=0, initial=0, label='seconds',
                                      help_text='Timeshift in second untils the parsing')

    def __init__(self, *args, **kwargs):
        kwargs['initial'] = kwargs.get('initial', {})
        if kwargs.get('instance'):
            # Fill in presentation-only fields
            kwargs['initial']['minute_delay'] = floor(int(kwargs['instance'].timeshift) / 60)
            kwargs['initial']['second_delay'] = int(kwargs['instance'].timeshift) % 60
        super(UrlQueueForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        object = super().save(commit=False)
        # Gather info from presentation fields into DB-field
        object.timeshift = self.cleaned_data.get('minute_delay', 0) * 60 + self.cleaned_data.get('second_delay', 0)
        if commit:
            object.save()
        return object


def activate(modeladmin, request, queryset):
    for item in queryset.filter(status='q'):
        settings.HTTP_CLIENT.fetch('%s/schedule?schedule_id=%d' % (settings.PARSING_URL, item.id))
activate.short_description = 'Send for processing'


class UrlQueueAdmin(admin.ModelAdmin):
    # List display
    list_display = ('url', 'timeshift', 'status', 'last_outcome_date')
    list_editable = ('status',)
    list_filter = ('status',)

    # One - item display
    form = UrlQueueForm  # Display with customization
    fieldsets = [
        (None, {'fields': ['url', 'status', 'last_outcome_date']}),
        ('Delay', {'fields': ['minute_delay', 'second_delay']})
    ]
    readonly_fields = ['last_outcome_date']
    inlines = [UrlParseResultInline]
    actions = [activate]


django.contrib.admin.site.register(UrlQueue, UrlQueueAdmin)


class UrlParseResultAdmin(admin.ModelAdmin):
    list_display = ('title', 'header', 'encoding')
    list_filter = ('encoding',)
    list_display_links = None


django.contrib.admin.site.register(UrlParseResult, UrlParseResultAdmin)
