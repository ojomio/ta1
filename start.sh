#!/bin/bash

python ./manage.py syncdb
python ./manage.py migrate
export PYTHONPATH=.
python ./manage.py runserver 127.0.0.1:12000&
echo $! Django
python ./url_parser/service.py --db_file=`readlink -f $(dirname $0)`/db.sqlite3 &
echo $! Tornado
