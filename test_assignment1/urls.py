from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView

from url_scheduler.views import MainView

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'test_assignment1.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^/?', MainView.as_view()),
)
