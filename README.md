Тестовое задание
=================
* Для запуска - установить требования `pip install -r ./requirements.txt`, затем `./start.sh`.
* Для входа в админку - создать суперпользователя `python ./manage.py createsuperuser`
* URL интерфейса [http://localhost:12000]
* Для отправки URLов на обработку - выделить созданные объекты в urlqueue и применить действие "Send to processing"
